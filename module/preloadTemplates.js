export const preloadHandlebarsTemplates = async function () {
    const templatePaths = [
        //Character Sheets
        'systems/dsa41/templates/character-sheet.html',
        //Actor partials
        'systems/dsa41/templates/partials/talents-tab.html',
        'systems/dsa41/templates/partials/talents-combat-tab.html',
        'systems/dsa41/templates/partials/talents-physical-tab.html',
        'systems/dsa41/templates/partials/talents-social-tab.html',
        'systems/dsa41/templates/partials/talents-nature-tab.html',
        'systems/dsa41/templates/partials/talents-knowledge-tab.html',
        'systems/dsa41/templates/partials/talents-crafting-tab.html',
        'systems/dsa41/templates/partials/talents-language-tab.html',
        'systems/dsa41/templates/partials/talents-gifts-tab.html',
        'systems/dsa41/templates/partials/talents-meta-tab.html',
        'systems/dsa41/templates/partials/attributes-tab.html',
        'systems/dsa41/templates/partials/combat-tab.html',
        'systems/dsa41/templates/partials/general-tab.html',
        'systems/dsa41/templates/partials/inventory-tab.html',
        'systems/dsa41/templates/partials/karmal-tab.html',
        'systems/dsa41/templates/partials/magic-tab.html',
        'systems/dsa41/templates/partials/notes-tab.html',
        'systems/dsa41/templates/partials/skills-tab.html',
    ];
    return loadTemplates(templatePaths);
};
