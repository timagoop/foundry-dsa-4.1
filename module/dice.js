export async function DSARoll({ parts = [], data = {}, options = {}, event = null, speaker = null, flavor = null, title = null, item = false}={}) {
	let rollMode = game.settings.get('core', 'rollMode');
	// Handle input arguments
	flavor = flavor || title;
	speaker = speaker || ChatMessage.getSpeaker();
	parts = parts.concat(["@bonus"]);
	let rolled = false;
  
	// Define inner roll function
	const _roll = function(parts, form=null) {
  
	  // Determine the d20 roll and modifiers
	  let nd = 1;
  
  
	  // Prepend the d20 roll
	  //let formula = `d20`;
	  //parts.unshift(formula);
  
  
  
	  // Execute the roll and flag critical thresholds on the d20
	  let roll = new Roll(parts.join(" + "), data).roll();
  
	//   Flag d20 options for any 20-sided dice in the roll
	  let ones = 0;
	  let twenties = 0;
	  for ( let d of roll.dice ) {
		if(d.results[0] == 1) {
			ones++;
		}
		if(d.results[0] == 20) {
			twenties++;
		}
	  }
  
  
	  // Convert the roll to a chat message and return the roll
	  rollMode = form ? form.rollMode.value : rollMode;
	  let success = "Misserfolg";
	  if(options.rollUnder) {
		success = ((roll.total <= options.target)  ? "Erfolg" : "Misserfolg");
	  }
	  else {
		success = ((roll.total >= options.target)  ? "Erfolg" : "Misserfolg");
	  }
	  if(ones >= 2) {
		  success = "Kritischer Erfolg";
	  }
	  if(twenties >= 2) {
		  success = "Kritischer Misserfolg";
	  }
	  roll.toMessage({
		speaker: speaker,
		flavor: flavor + " " + success
	  }, { rollMode });
	  rolled = true;
	  return roll;
	};
  
	// Determine whether the roll can be fast-forward
	// if ( fastForward === null ) {
	//   fastForward = event && (event.shiftKey || event.altKey || event.ctrlKey || event.metaKey);
	// }
  
	// Optionally allow fast-forwarding to specify advantage or disadvantage
	//if ( fastForward ) {
	  return _roll(parts, 0);
	//}
  
	// // Render modal dialog
	// template = template || "systems/dnd5e/templates/chat/roll-dialog.html";
	// let dialogData = {
	//   formula: parts.join(" + "),
	//   data: data,
	//   rollMode: rollMode,
	//   rollModes: CONFIG.Dice.rollModes,
	//   config: CONFIG.DND5E
	// };
	// const html = await renderTemplate(template, dialogData);
  
	// // Create the Dialog window
	// let roll;
	// return new Promise(resolve => {
	//   new Dialog({
	// 	title: title,
	// 	content: html,
	// 	buttons: {
	// 	  advantage: {
	// 		label: game.i18n.localize("DND5E.Advantage"),
	// 		callback: html => roll = _roll(parts, 1, html[0].children[0])
	// 	  },
	// 	  normal: {
	// 		label: game.i18n.localize("DND5E.Normal"),
	// 		callback: html => roll = _roll(parts, 0, html[0].children[0])
	// 	  },
	// 	  disadvantage: {
	// 		label: game.i18n.localize("DND5E.Disadvantage"),
	// 		callback: html => roll = _roll(parts, -1, html[0].children[0])
	// 	  }
	// 	},
	// 	default: "normal",
	// 	close: html => {
	// 	  if (onClose) onClose(html, parts, data);
	// 	  resolve(rolled ? roll : false)
	// 	}
	//   }, dialogOptions).render(true);
	// })
  }
  