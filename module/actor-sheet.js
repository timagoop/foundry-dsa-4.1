
import { DSARoll } from "./dice.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class Dsa41ActorSheet extends ActorSheet {

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["dsa41", "sheet", "actor"],
  	  template: "systems/dsa41/templates/character-sheet.html",
      width: 600,
      height: 600,
      tabs: [
        {navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "basevalues"},
        {navSelector: ".talents-tabs", contentSelector: ".talents-body", initial: "combat"}    
        ],
      dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
    });
  }

  /* -------------------------------------------- */

  _checkNull(items) {
    if (items && items.length) {
        return items;
    }
    return [];
  }
  /** @override */
  getData() {
    const data = super.getData();

    data.itemsByType = {};
    for (const item of data.items) {
        let list = data.itemsByType[item.type];
        if (!list) {
            list = [];
            data.itemsByType[item.type] = list;
        }
        list.push(item);
    }
    data.talentsByCategory = {};
    for (const talent of this._checkNull(data.itemsByType['talent'])) {
        let list = data.talentsByCategory[talent.data.category];
        if (!list) {
            list = [];
            data.talentsByCategory[talent.data.category] = list;
        }
        list.push(talent);
    }
    data.dtypes = ["String", "Number", "Boolean"];
    data.data.owned = {};
    data.data.owned.talents = {};
    data.data.owned.talents.combat = this._checkNull(data.talentsByCategory['combat']);
    data.data.owned.talents.physical = this._checkNull(data.talentsByCategory['physical']);
    data.data.owned.talents.social = this._checkNull(data.talentsByCategory['social']);
    data.data.owned.talents.nature = this._checkNull(data.talentsByCategory['nature']);
    data.data.owned.talents.knowledge = this._checkNull(data.talentsByCategory['knowledge']);
    data.data.owned.talents.crafting = this._checkNull(data.talentsByCategory['crafting']);
    data.data.owned.spells = this._checkNull(data.itemsByType['spell']);
    // for ( let attr of Object.values(data.data.attributes) ) {
    //   attr.isCheckbox = attr.dtype === "Boolean";
    // }
    // data.data.derived = {};
    // data.data.derived.life_energy = this.actor.lifeEnergy();
    return data;
  }

  /* -------------------------------------------- */

  /** @override */
	activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Add or Remove Attribute
    // html.find(".attributes").on("click", ".attribute-control", this._onClickAttributeControl.bind(this));


    // Roll attribute
      html.find('.attribute-key').click((event) => {
        let actorObject = this.actor;
        let element = event.currentTarget;
        let attribute = element.name;
        actorObject.rollAttribute(attribute, { event: event });
    });

    // Roll talent
      html.find('.talent-key').click((event) => {
        let actorObject = this.actor;
        let element = event.currentTarget;
        let talent = element.name;
        actorObject.rollTalent(talent, { event: event });
    });

    // Roll spell
      html.find('.spell-key').click((event) => {
        let actorObject = this.actor;
        let element = event.currentTarget;
        let spell = element.name;
        actorObject.rollTalent(spell, { event: event });
    });


    // Roll combat skill
    html.find('.combat-skill button').click((event) => {
      let actorObject = this.actor;
      let element = event.currentTarget;
      let skill = element.name;
      actorObject.rollCombatSkill(skill, { event: event });
  });

  // Roll hitpoints skill
  html.find('.hitpoints-id').click((event) => {
    DSARoll({
      event: event,
      parts: [this.actor.data.data.combat.TP],
      data: this.actor.data,
      speaker: ChatMessage.getSpeaker({ actor: this }),
      flavor: "TP",
      title: "TP"
  })
});
  }

  /* -------------------------------------------- */

  /** @override */
  setPosition(options={}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - 192;
    sheetBody.css("height", bodyHeight);
    return position;
  }

  /* -------------------------------------------- */

  /**
   * Listen for click events on an attribute control to modify the composition of attributes in the sheet
   * @param {MouseEvent} event    The originating left click event
   * @private
   */
  async _onClickAttributeControl(event) {
    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;
    const attrs = this.object.data.data.attributes;
    const form = this.form;

    // Add new attribute
    // if ( action === "create" ) {
    //   const nk = Object.keys(attrs).length + 1;
    //   let newKey = document.createElement("div");
    //   newKey.innerHTML = `<input type="text" name="data.attributes.attr${nk}.key" value="attr${nk}"/>`;
    //   newKey = newKey.children[0];
    //   form.appendChild(newKey);
    //   await this._onSubmit(event);
    // }

    // // Remove existing attribute
    // else if ( action === "delete" ) {
    //   const li = a.closest(".attribute");
    //   li.parentElement.removeChild(li);
    //   await this._onSubmit(event);
    // }
  }

  /* -------------------------------------------- */

  /** @override */
  async _updateObject(event, formData) {

    // // Handle the free-form attributes list
    // const formAttrs = expandObject(formData).data.attributes || {};
    // const attributes = Object.values(formAttrs).reduce((obj, v) => {
    //   let k = v["key"].trim();
    //   if ( /[\s\.]/.test(k) )  return ui.notifications.error("Attribute keys may not contain spaces or periods");
    //   delete v["key"];
    //   obj[k] = v;
    //   return obj;
    // }, {});
    
    // // Remove attributes which are no longer used
    // for ( let k of Object.keys(this.object.data.data.attributes) ) {
    //   if ( !attributes.hasOwnProperty(k) ) attributes[`-=${k}`] = null;
    // }

    // // Re-combine formData
    // formData = Object.entries(formData).filter(e => !e[0].startsWith("data.attributes")).reduce((obj, e) => {
    //   obj[e[0]] = e[1];
    //   return obj;
    // }, {_id: this.object._id, "data.attributes": attributes});
    const formOwnedItems = expandObject(formData).data.owned;
    for ( let k of Object.keys(formOwnedItems) ) {
      //let item = this.actor.getOwnedItem(k);
      const update = {_id: k, data: {value: formOwnedItems[k].data.value}};
      await this.actor.updateOwnedItem(update);
    }    
    formData = Object.entries(formData).filter(e => !e[0].startsWith("data.owned")).reduce((obj, e) => {
        obj[e[0]] = e[1];
        return obj;
      }, {_id: this.object._id, "data.owned": {}});
    // Update the Actor
    return this.object.update(formData);
  }



}
